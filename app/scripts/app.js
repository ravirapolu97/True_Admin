'use strict';

var trueAdmin = angular.module('trueAdminApp', [
	'ui.router',
  // 'ui.router.stateHelper',
	'oc.lazyLoad'
]);

// Including States
trueAdmin.config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/login");
    $stateProvider
    // Initial state
    .state ('login', {
      	url: '/login',
      	templateUrl: 'app/views/login.html',
      	data: {pageTitle: 'Login'},
      	controller: "LoginController",
        controllerAs: 'vm',
      	resolve: {
	      deps: ['$ocLazyLoad', function($ocLazyLoad) {
	        return $ocLazyLoad.load({
	          files: [
	            'app/scripts/controllers/loginCtrl.js'
	          ]
	        });
	      }]
    	}
    })
    // common parent state
    .state ('home', {
      url: '/home',
      abstract: true,
      templateUrl: 'app/views/common.html'
    })
    .state ('home.user', {
      url: '/user',
      templateUrl: 'app/views/user.html',
      data: {pageTitle: 'User'},
      controller: "UserController",
      controllerAs: 'uc',
      resolve: {
      deps: ['$ocLazyLoad', function($ocLazyLoad) {
        return $ocLazyLoad.load({
          files: [
            'app/scripts/controllers/userCtrl.js'
          ]
        });
      }]
    }
  })
});


trueAdmin.run(['$state',function($state){

}]);